read EEPROM contents to a file:
  avrdude -p atmega32u4 -c avr109 -b 57600 -D -P /dev/ttyACM0 -U eeprom:r:eeprom.hex:i
  (need to ground RST twice to enter bootloader mode)

write to EEPROM:
  avrdude -p atmega32u4 -c avr109 -b 57600 -D -P /dev/ttyACM0 -U eeprom:w:eeprom.hex:i
  (need to ground RST twice to enter bootloader mode)
