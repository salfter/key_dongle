#!/usr/bin/env bash
echo -en "0.1\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0\x0Set me up on my serial configuration interface...9600 bps, 8/N/1\x0\x0\x0\x0" >eeprom.bin
avrdude -c usbasp -P usb -p m32u4 -e -U flash:w:.pio/build/sparkfun_promicro16/firmware.hex -U eeprom:w:eeprom.bin:r -U lfuse:w:0xde:m -U hfuse:w:0x91:m -U efuse:w:0xf3:m
