$fn=45;

// additional thickness

t=0.4;

// may also need to open up button holes a bit more

t2=.1;

// one-piece adjustment: board should be flush with edge of print

opa=0.4;

// one-piece extra: more layers on top

ope=0.8;

// partset() renders all parts ready to print
// mockup() 

one_piece();
//partset();
//mockup();

module one_piece()
{
    rotate([180,0,0])
    difference()
    {
        union()
        {
            // top, with possible extensions
            top(0);
            translate([-.7,-1.7-t,-.6-t-opa])
                cube([53.67,1+t,4.6+2*t]);
            translate([-.7,23.275,-.6-t-opa])
                cube([53.67,1+t,4.6+2*t]);
            translate([-1.7-t,-1.7-t,-.6-t-opa])
                cube([1+t,25.975+2*t,4.6+2*t-opa]);
            translate([52.97,-1.7-t,-.6-t-opa])
                cube([1+t,7+t,4.6+2*t]);
            translate([52.97,17.275,-.6-t-opa])
                cube([1+t,7+t,4.6+2*t]);
            
            // bottom
            translate([0,0,-opa])
                bottom(0.2);
            translate([-.7-(1+t),-.7-(1+t),-opa-ope-1])
                cube([53.67+2*(1+t),23.975+2*(1+t),ope]);
            translate([-.7-(1+t),-.7-(1+t),-opa-1])
                cube([53.67+2*(1+t),23.975+2*(1+t),.4]);

            // "nubs" to press the button switches
            translate([8.2,5.4,2])
                cylinder(d=3, h=.5);
            translate([8.2,5.4+11.8,2])
                cylinder(d=3, h=.5);
            translate([8.2+12,5.4,2])
                cylinder(d=3, h=.5);
            translate([8.2+12,5.4+11.8,2])
                cylinder(d=3, h=.5);
        }

        // button labels
        translate([8.2,5.4,3.45+t])
        rotate([0,0,-90])
        linear_extrude(0.6, convexity=10)
            text("D", size=8, halign="center", valign="center", font="Bahnschrift:style:Bold");
        translate([8.2,5.4+11.8,3.45+t])
        rotate([0,0,-90])
        linear_extrude(0.6, convexity=10)
            text("C", size=8, halign="center", valign="center", font="Bahnschrift:style:Bold");
        translate([8.2+12,5.4,3.45+t])
        rotate([0,0,-90])
        linear_extrude(0.6, convexity=10)
            text("B", size=8, halign="center", valign="center", font="Bahnschrift:style:Bold");
        translate([8.2+12,5.4+11.8,3.45+t])
        rotate([0,0,-90])
        linear_extrude(0.6, convexity=10)
            text("A", size=8, halign="center", valign="center", font="Bahnschrift:style:Bold");
    }
}

module partset()
{
    translate([.5,23,0])
    rotate([180,0,0])
        bottom(0);

    translate([1.7,-3-2*t,4+t])
    rotate([180,0,0])
        top();

    translate([5,30,0])
    rotate([0,0,90])
        button("A");

    translate([15,30,0])
    rotate([0,0,90])
        button("B");

    translate([25,30,0])
    rotate([0,0,90])
        button("C");

    translate([35,30,0])
    rotate([0,0,90])
        button("D");
}

module mockup()
{
    translate([66.94,22.575,1.6])
    rotate([0,0,180])
        %import("../kicad/key_dongle.stl");

    bottom(0);
    top();
    translate([8.2,5.4,2.5])
        button("D");
    translate([8.2,5.4+11.8,2.5])
        button("C");
    translate([8.2+12,5.4,2.5])
        button("B");
    translate([8.2+12,5.4+11.8,2.5])
        button("A");
}

module bottom(gap)
{
    translate([-.5-gap,-.5-gap,-.6-t])
        cube([53.47+2*gap,23.575+2*gap,0.6+t]);
    translate([52.9,5.4,-.6])
        cube([14,11.8,0.6]);
}

module top(make_holes)
{
    difference()
    {
        union()
        {
            // top
            translate([-.7,-.7,3])
                cube([53.67,23.975,1+t]);

            // keychain loop

            translate([-1.5-t,4.3-t,1+t])
            difference()
            {
                cylinder(d=12, h=3);
                cylinder(d=6, h=3);
                translate([0,-6,0])
                    cube([12,12,3]);
            }

            // standoffs
            translate([.25,.25,1.6])
                cube([2,2,1.5]);
            translate([.25,20.325,1.6])
                cube([2,2,1.5]);
            translate([50.97,.25,1.6])
                cube([2,2,1.5]);
            translate([50.97,20.325,1.6])
                cube([2,2,1.5]);
            translate([25.97,.25,1.6])
                cube([2,22.075,1.5]);

            // sides
            translate([-.7,-1.7-t,-.6-t])
                cube([53.67,1+t,4.6+2*t]);
            translate([-.7,23.275,-.6-t])
                cube([53.67,1+t,4.6+2*t]);
            translate([-1.7-t,-1.7-t,-.6-t])
                cube([1+t,25.975+2*t,4.6+2*t]);
            translate([52.97,-1.7-t,-.6-t])
                cube([1+t,7+t,4.6+2*t]);
            translate([52.97,17.275,-.6-t])
                cube([1+t,7+t,4.6+2*t]);
            translate([52.97,-1.7-t,1.7])
                cube([1+t,25.975+2*t,2.3+t]);
        }

        if (make_holes!=0)
        {
            // button holes
            translate([8.2,5.4,2.5])
                button_hole();
            translate([8.2,5.4+11.8,2.5])
                button_hole();
            translate([8.2+12,5.4,2.5])
                button_hole();
            translate([8.2+12,5.4+11.8,2.5])
                button_hole();
        }
    }
}

module button(label)
{
    rotate([0,0,-90])
    difference()
    {
        rotate([0,0,90])
        union()
        {
            cylinder(d=8, h=.8);
            translate([-.75,0,0])
            {
                cylinder(d=5, h=2.5+t);
                translate([0,-2.5,0])
                    cube([1.5,5,2.5+t]);
            }
            translate([.75,0,0])
                cylinder(d=5, h=2.5+t);
        }
        translate([0,0,1.9+t])
        linear_extrude(0.6, convexity=10)
            text(label, size=4.5, halign="center", valign="center", font="Bahnschrift:style:Bold");
   }
}

module button_hole()
{
    translate([-.75,0.0])
    {
        cylinder(d=5.3+2*t2, h=2+t);
        translate([0,-2.65-t2,0])
            cube([1.5,5.3+2*t2,2+t]);
    }
    translate([.75,0.0])
        cylinder(d=5.3+2*t2, h=2+t);
}