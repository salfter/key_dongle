#include <Arduino.h>
#include <Keyboard.h>
#include <EEPROM.h>

// configuration

const int slotcount=4; // number of keys to support
String passwords[slotcount]; // password storage
int pins[slotcount]={2,3,4,5}; // pins to which keys are attached

// other globals

String command=""; // serial-port command buffer
const String version="0.1"; // firmware version

// prototypes

void setup();
void loop();
void SendPassword(int slot);
void LoadPasswords();
void SavePasswords();
void Help();
void ShowPassword(int key);
void ShowAll();
void ShowVersion();
String ReadHWVersion();

// initialization

void setup() 
{
  SerialUSB.begin(9600);
  SerialUSB.write(']');

  // set up input pins

  for (int slot=0; slot<slotcount; slot++)
    pinMode(pins[slot], INPUT_PULLUP);

  // load passwords

  LoadPasswords();
}

// event loop

void loop() 
{
  char inchr;
  bool cmdComplete=false;

  // scan keys

  for (int slot=0; slot<slotcount; slot++)
    if (!digitalRead(pins[slot]))
      SendPassword(slot);

  // grab serial-port input

  if (SerialUSB.available()>0)
  {
    inchr=SerialUSB.read();
    if (inchr>=32 && inchr<127) // printable?
    {
      command+=inchr;
      SerialUSB.write(inchr); // echo it back
    }
    if (inchr==8 || inchr==127) // delete or backspace?
      if (command.length()>0)
      {
        command=command.substring(0,command.length()-1);
        SerialUSB.write(8); // erase last character
        SerialUSB.write(32);
        SerialUSB.write(8);
      }
    if (inchr==13) // return?
    {
      cmdComplete=true; // end of line
      SerialUSB.write("\r\n");
    }
  }
  if (cmdComplete) // EOL?
  {
    if (command=="")
      Help();
    else if (command.substring(0,5)=="show ")
    {
      command=command.substring(5);
      if (command=="all")
        ShowAll();
      else
        ShowPassword(command.toInt());
    }
    else if (command.substring(0,4)=="set ")
    {
      command=command.substring(4);
      int slot=0;
      while (command[0]!=' ')
      {
        slot=10*slot+String(command[0]).toInt();
        command=command.substring(1);
      }
      command=command.substring(1);
      if (slot>=0 && slot<slotcount)
      {
        passwords[slot]=command;
        SavePasswords();
        ShowPassword(slot);
      }
      else
      {
        SerialUSB.write(7);
        SerialUSB.println("?RANGE ERROR");
      }
    }
    else if (command=="version")
    {
      ShowVersion();
    }
    else
    {
      SerialUSB.write(7); 
      SerialUSB.println("?SYNTAX ERROR"); // Apple II-style error messages, because we can :-)
    }

    command="";
    SerialUSB.write(']'); // and an Apple II-style prompt, too
  }
}

void SendPassword(int slot)
{
    for (int i=0; i<passwords[slot].length(); i++)
    {
      Keyboard.press(passwords[slot][i]);
      Keyboard.release(passwords[slot][i]);
    }
    delay(1000);
}

// read passwords from EEPROM

void LoadPasswords()
{
  int memptr=16;
  char c;

  for (int slot=0; slot<slotcount; slot++)
  {
    passwords[slot]="";
    while ((c=EEPROM.read(memptr++))!=0)
      passwords[slot]+=c;
  }
}

// write passwords to EEPROM

void SavePasswords()
{
  int memptr=16;
  for (int slot=0; slot<slotcount; slot++)
  {
    for (int i=0; i<passwords[slot].length(); i++)
      EEPROM.write(memptr++,passwords[slot][i]);
    EEPROM.write(memptr++,0);
  }
}

// display help on an empty input

void Help()
{
  SerialUSB.println("Help\r\n====\r\n");
  SerialUSB.println("set n data    set password for key n to data");
  SerialUSB.println("show n        show password for key n");
  SerialUSB.println("show all      show all passwords");
  SerialUSB.println("version       show hardware & firmware information\r\n");
  SerialUSB.println("(n starts at 0 and goes to one less than the total");
  SerialUSB.println("number of keys\r\n");
}

void ShowPassword(int key)
{
  if (key>=0 && key<slotcount)
    SerialUSB.println("pwd "+String(key)+": "+passwords[key]);
  else
  {
    SerialUSB.write(7);
    SerialUSB.println("?RANGE ERROR");
  }
}

void ShowAll()
{
  for (int i=0; i<slotcount; i++)
    ShowPassword(i);
}

void ShowVersion()
{
  SerialUSB.println("Firmware version: "+version);
  SerialUSB.println("Hardware version: "+ReadHWVersion());
  SerialUSB.println("Total keys configured: "+String(slotcount));
}

String ReadHWVersion()
{
  int memptr=0;
  char c;
  String version="";

  while ((c=EEPROM.read(memptr++))!=0)
    version+=c;
  return version;
}
